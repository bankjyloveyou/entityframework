﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
         
                switchs();
            
            
        }

        static void switchs()
        {
            Console.WriteLine("1. show");
            Console.WriteLine("2. Select COUNTRY:US");
            Console.WriteLine("3. Insert ID:008");
            Console.WriteLine("4. Update ID:008");
            Console.WriteLine("5. Delete ID:008");
            Console.WriteLine("0. Exit");
            var n = Console.ReadLine();
            

            switch (n)
            {
                case "1":
                    showdata();
                    break;
                case "2":
                    select();
                    break;
                case "3":
                    insert();
                    break;
                case "4":
                    update();
                    break;
                case "5":
                    delete();
                    break;
                case "0":
                    break;
            }
        }

        static void showdata()
        {
            using (var db = new DB_LearningEntities())
            {
                var ds = db.CUSTOMER.ToList();
                if (ds.Count() > 0)
                 {
                    foreach (var item in ds)
                    {
                        Console.WriteLine($"ID:{item.CUSTOMER_ID} | Name:{item.NAME} | Email:{item.EMAIL} | Country:{item.COUNTRY_CODE} | Budget:{item.BUDGET} | Used:{item.USED}");
                    }
                }
                
            }
            switchs();
        }
        static void select()
        {
            using (var db = new DB_LearningEntities())
            {
                var ds = (from c in db.CUSTOMER
                          join d in db.COUNTRY on c.COUNTRY_CODE equals d.COUNTRY_CODE
                          where c.COUNTRY_CODE.Contains("US")
                          select new
                          {
                              CusID = c.CUSTOMER_ID,
                              CusName = c.NAME,
                              CusEmail = c.EMAIL,
                              CountryName = d.COUNTRY_NAME,
                              CusBudget = c.BUDGET,
                              CusUsed = c.USED
                          }).ToList();
                if (ds.Count() > 0)
                {
                    foreach (var item in ds)
                    {
                        Console.WriteLine($"ID:{item.CusID} | Name:{item.CusName} | Email:{item.CusEmail} | Country:{item.CountryName} | Budget:{item.CusBudget} | Used:{item.CusUsed}");
                    }

                }
                switchs();
            }
        }
        static void insert()
        {
            using (var db = new DB_LearningEntities())
            {
               
                var insert = new CUSTOMER();
                insert.CUSTOMER_ID = "C008";
                insert.NAME = "Name Insert";
                insert.EMAIL = "Insert@gmail.com";
                insert.COUNTRY_CODE = "TH";
                insert.BUDGET = 9999999;
                insert.USED = 555555;
                db.CUSTOMER.Add(insert);
                db.SaveChanges();


                showdata();
                
            }
        }
        static void update()
        {
            using (var db = new DB_LearningEntities())
            {
                // Update CUSTOMER
                string strCustomerID = "C008";

                // Update Statement
                var update = db.CUSTOMER.Where(o => o.CUSTOMER_ID == strCustomerID).FirstOrDefault();
                if (update != null)
                {
                    update.NAME = "name Update";
                    update.EMAIL = "Update@gmail.com";
                    update.BUDGET = 12222222;
                    update.USED = 111111;
                }
                db.SaveChanges();

                showdata();
                
            }

        }
        static void delete()
        {
            using (var db = new DB_LearningEntities())
            {
                // Delete CUSTOMER
                string strCustomerID = "C008";

                // Delete CUSTOMER
                var delete = db.CUSTOMER.Where(o => o.CUSTOMER_ID == strCustomerID).FirstOrDefault();
                if (delete != null)
                {
                    db.CUSTOMER.Remove(delete);
                }
                db.SaveChanges();

                showdata();
                
            }

        }

    }
}
